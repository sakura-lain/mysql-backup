#!/bin/bash

#NAME=$(echo "${base}_$(date '+%F_%T' | sed 's/://g').sql") #Nom de la base sous la forme nom_date_heure.sql

#Nom BDD
DB=''
DATE=$(date '+%F')
TIME=$(date '+%T' | sed 's/://g')
NAME=$(echo "${DB}_${DATE}_${TIME}.sql")
FOLDER=$(echo $NAME | tr -d ".sql")
DATABASES=$(MYSQL_PWD=$DB_PASS mysql -u $DB_USER -e "SHOW DATABASES;" | tr -d "| " | grep -v -e Database -e _schema -e mysql)

#Exportation BDD
#MYSQL_PWD="mot_de_passe" mysqldump -u utilisateur base > $NAME 

#Comptage nb de svg

#ls -l | wc -l   
#find . -name \* | wc -l


folder(){
	mkdir $FOLDER
}


backup(){
	for DB_NAME in $DATABASES
	do
		MYSQL_PWD=$DB_PASS mysqldump -u $DB_USER --single-transaction --skip-lock-tables $DB_NAME -h $DB_HOST > $FOLDER/$DB_NAME.sql
		gzip $FOLDER/$DB_NAME.sql
	done

}


remove(){
	OUTDIR=$(find . -name $NAME\*.sql | sort -n)
	set $OUTDIR

	if [ $# -gt 5 ]
	then 
		rm -r $1
	fi
}





