#!/bin/bash

#NAME=$(echo "${base}_$(date '+%F_%T' | sed 's/://g').sql") #Nom de la base sous la forme nom_date_heure.sql

#Nom BDD
#DB=''
export(){
	DATE=$(date '+%F' | sed 's/-//g')
	TIME=$(date '+%T' | sed 's/://g')
	#NAME=$(echo "${DB}_${DATE}_${TIME}.sql")
	DB_PASS="mysql_password"
	DB_USER="mysql_user"
	DB_HOST="localhost"
	DATABASES=$(MYSQL_PWD=$DB_PASS mysql -u $DB_USER -e "SHOW DATABASES;" | tr -d "| " | grep -v -e Database -e _schema -e mysql)
	set $DATABASES

	#Exportation BDD
	for DB in $@
	do
		outdir
		DB_NAME=$(echo "${DB}_${DATE}_${TIME}.sql")
		MYSQL_PWD=$DB_PASS mysqldump -u $DB_USER --single-transaction --skip-lock-tables $DB -h $DB_HOST > $OUTDIR/$DB_NAME
		remove
		archive
	done
}

#Comptage nb de svg

#ls -l | wc -l   
#find . -name \* | wc -l


remove(){
	BASES=$(find . -name $DB\*.sql | sort -n)
	set $BASES

	if [[ $# -gt 5 ]]
	then 
		rm $1
	fi
}

outdir(){
	OUTDIR=$DB
	if [[ ! -d $OUTDIR ]]
	then
		mkdir $OUTDIR
	fi
}

#archivage incrémental de $OUTDIR
archive(){
	LIST="$DB.list"
	TAR_NAME=$(echo "$DB_NAME.tar" | sed 's/.sql//g')
	tar --create --file=$TAR_NAME --listed-incremental=$LIST $OUTDIR
}
		
export
#remove



